package club.bugmkaers.demospringbootmongodb;

import club.bugmkaers.demospringbootmongodb.demo.model.MessageModel;
import club.bugmkaers.demospringbootmongodb.demo.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class DemoSpringbootMongodbApplicationTests {

    @Autowired
    private MessageRepository repository;

    @Test
    public void contextLoads() {
        List<MessageModel> all = repository.findAll();
        log.info("all: {}", all);
    }

}
