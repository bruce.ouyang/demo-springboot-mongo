package club.bugmkaers.demospringbootmongodb.tools;

import org.junit.Test;
import org.lionsoul.ip2region.DbConfig;
import org.lionsoul.ip2region.DbMakerConfigException;
import org.lionsoul.ip2region.DbSearcher;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @ClassName: Ip2RegionTest
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 17:25
 * @Version: V1
 **/
public class Ip2RegionTest {

    @Test
    public void test() {

        // 参考：https://gitee.com/lionsoul/ip2region

        String dbFile = "D:\\bruce\\bruce-gitlab\\demo-springboot-mongo\\src\\main\\resources\\static\\ip2region.db";
        long begin = System.currentTimeMillis();
        String ip = "222.244.202.245";
        try {
            DbSearcher dbSearcher = new DbSearcher(new DbConfig(), dbFile);
            String region = dbSearcher.binarySearch(ip).getRegion();
            System.out.println(ip + " 转地址结果：" + region);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DbMakerConfigException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("耗时：" + (System.currentTimeMillis() - begin) + " ms");
    }
}
