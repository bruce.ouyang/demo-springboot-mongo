package club.bugmkaers.demospringbootmongodb.tools;

import com.ip2location.IP2Location;
import com.ip2location.IPResult;
import org.junit.Test;

import java.io.IOException;

/**
 * @ClassName: Ip2LocationTest
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 16:46
 * @Version: V1
 **/
public class Ip2LocationTest {

    @Test
    public void test(){
        IP2Location ip2Location = new IP2Location();
        ip2Location.IPDatabasePath = "D:\\bruce\\bruce-gitlab\\demo-springboot-mongo\\src\\main\\resources\\static\\IP2LOCATION-LITE-DB11.BIN";
        IPResult res = null;
        try {
            res = ip2Location.IPQuery("101.230.3.2");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if("OK".equals(res.getStatus())){
          System.out.println(res);
          System.out.println(res.getCountryLong());
        }
    }
}
