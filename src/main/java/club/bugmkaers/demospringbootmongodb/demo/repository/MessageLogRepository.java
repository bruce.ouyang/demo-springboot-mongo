package club.bugmkaers.demospringbootmongodb.demo.repository;

import club.bugmkaers.demospringbootmongodb.demo.model.MessageLog;
import club.bugmkaers.demospringbootmongodb.demo.model.MessageModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: MessageRepository
 * @Description: TODO
 * @Author: bruce.ouyang
 * @Date: 2020/8/9 10:51 下午
 * @Version: v1.0
 */
@Repository
public interface MessageLogRepository extends MongoRepository<MessageLog, Long> {
}
