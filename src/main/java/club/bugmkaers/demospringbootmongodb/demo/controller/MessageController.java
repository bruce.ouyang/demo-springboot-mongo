package club.bugmkaers.demospringbootmongodb.demo.controller;

import club.bugmkaers.demospringbootmongodb.demo.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: MessageController
 * @Description: TODO
 * @Author: bruce.ouyang
 * @Date: 2020/8/9 10:31 下午
 * @Version: v1.0
 */
@RestController
@RequestMapping("/msg")
public class MessageController {

    @Autowired
    private MessageService service;

    @GetMapping("/each")
    public String each() {
        long cost = service.saveEach();
        return "success, cost: " + cost + " ms";
    }

    @GetMapping("/batch")
    public String batch() {
        long cost = service.saveBatch();
        return "success, cost: " + cost + " ms";
    }
}
