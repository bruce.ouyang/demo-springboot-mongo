package club.bugmkaers.demospringbootmongodb.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @ClassName: MessageLog
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 13:36
 * @Version: V1
 **/
@Document(
        collection = "sns_ecoSaaS_message_log"
)
public class MessageLog extends BaseEntity {
    @Field("message")
    private Message message;
    @Field("to_user")
    private User toUser;
    @Field("copy_user")
    private User copyUser;
    @Field("tenant_id")
    private String tenantId;
    @Field("is_receive")
    private Integer isReceive;
    @Field("is_copy")
    private Integer isCopy;
    @Field("is_read")
    private Integer isRead;
    @Field("is_team")
    private Integer isTeam;
    @Field("ep_tenant_id")
    private String epTenantId;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public User getCopyUser() {
        return copyUser;
    }

    public void setCopyUser(User copyUser) {
        this.copyUser = copyUser;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getIsReceive() {
        return isReceive;
    }

    public void setIsReceive(Integer isReceive) {
        this.isReceive = isReceive;
    }

    public Integer getIsCopy() {
        return isCopy;
    }

    public void setIsCopy(Integer isCopy) {
        this.isCopy = isCopy;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public Integer getIsTeam() {
        return isTeam;
    }

    public void setIsTeam(Integer isTeam) {
        this.isTeam = isTeam;
    }

    public String getEpTenantId() {
        return epTenantId;
    }

    public void setEpTenantId(String epTenantId) {
        this.epTenantId = epTenantId;
    }
}
