package club.bugmkaers.demospringbootmongodb.demo.model;

import com.alibaba.fastjson.JSONObject;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * @ClassName: Message
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 13:37
 * @Version: V1
 **/

@Document(
        collection = "sns_ecoSaaS_message"
)
public class Message extends BaseEntity {
    @Field("message")
    private JSONObject message;
    @Field("type")
    private Integer type;
    @Field("ep_tenant_id")
    private String epTenantId;
    @Field("from")
    private User from;
    @Field("to")
    private List<JSONObject> to;
    @Field("tenant_id")
    private String tenantId;
    @Field("start_time")
    private String startTime;
    @Field("end_time")
    private String endTime;
    @Field("is_loop")
    private Integer isLoop;
    @Transient
    private Integer praiseCount;
    @Transient
    private Integer isPraise;
    @Field("praise")
    private List<User> praise;
    @Transient
    private List<Comment> commentary;
    @Transient
    private Integer commentCount;
    @Field("to_user")
    private User toUser;
    @Field("is_push")
    private Integer isPush;
    @Field("is_team")
    private Integer isTeam;
    @Field("is_active_msg")
    private Integer isActiveMsg;

    public JSONObject getMessage() {
        return message;
    }

    public void setMessage(JSONObject message) {
        this.message = message;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getEpTenantId() {
        return epTenantId;
    }

    public void setEpTenantId(String epTenantId) {
        this.epTenantId = epTenantId;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public List<JSONObject> getTo() {
        return to;
    }

    public void setTo(List<JSONObject> to) {
        this.to = to;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getIsLoop() {
        return isLoop;
    }

    public void setIsLoop(Integer isLoop) {
        this.isLoop = isLoop;
    }

    public Integer getPraiseCount() {
        return praiseCount;
    }

    public void setPraiseCount(Integer praiseCount) {
        this.praiseCount = praiseCount;
    }

    public Integer getIsPraise() {
        return isPraise;
    }

    public void setIsPraise(Integer isPraise) {
        this.isPraise = isPraise;
    }

    public List<User> getPraise() {
        return praise;
    }

    public void setPraise(List<User> praise) {
        this.praise = praise;
    }

    public List<Comment> getCommentary() {
        return commentary;
    }

    public void setCommentary(List<Comment> commentary) {
        this.commentary = commentary;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public Integer getIsPush() {
        return isPush;
    }

    public void setIsPush(Integer isPush) {
        this.isPush = isPush;
    }

    public Integer getIsTeam() {
        return isTeam;
    }

    public void setIsTeam(Integer isTeam) {
        this.isTeam = isTeam;
    }

    public Integer getIsActiveMsg() {
        return isActiveMsg;
    }

    public void setIsActiveMsg(Integer isActiveMsg) {
        this.isActiveMsg = isActiveMsg;
    }
}
