package club.bugmkaers.demospringbootmongodb.demo.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * @ClassName: Comment
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 13:42
 * @Version: V1
 **/
@Document(
        collection = "sns_ecoSaaS_comment"
)
public class Comment extends BaseEntity {
    @Field("tenant_id")
    private String tenantId;
    @Field("message_id")
    private String messageId;
    @Field("content")
    private String content;
    @Field("from_user")
    private User fromUser;
    @Field("to_user")
    private List<User> toUser;
    @Field("opera_type")
    private Integer operaType;
    @Field("ep_tenant_id")
    public String epTenantId;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public List<User> getToUser() {
        return toUser;
    }

    public void setToUser(List<User> toUser) {
        this.toUser = toUser;
    }

    public Integer getOperaType() {
        return operaType;
    }

    public void setOperaType(Integer operaType) {
        this.operaType = operaType;
    }

    public String getEpTenantId() {
        return epTenantId;
    }

    public void setEpTenantId(String epTenantId) {
        this.epTenantId = epTenantId;
    }
}
