package club.bugmkaers.demospringbootmongodb.demo.model;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @ClassName: User
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 13:40
 * @Version: V1
 **/
public class User {
    @Field("emp_id")
    private String empId;
    @Field("person_id")
    private String personId;
    @Field("user_id")
    private String userId;
    @Field("user_name")
    private String userName;
    @Field("english_name")
    private String englishName;
    @Field("nick_name")
    private String nickName;
    @Field("dep_id")
    private String depId;
    @Field("dep_name")
    private String depName;
    @Field("dep_english_name")
    private String depEnglishName;
    @Field("sex")
    private Integer sex;
    @Field("employee_no")
    private String employeeNo;
    @Field("tenant_id")
    private String tenantId;
    @Field("photo")
    private String photo;
    @Field("object_name")
    private String objectName;
    @Field("phone")
    private String phone;
    @Field("email")
    private String email;
    @Field("position")
    private String positionName;
    @Field("position_english_name")
    private String positionEnglishName;
    @Field("sbordinate_id")
    private String subordinateId;
    private String onboardingDate;
    @Field("app_lang")
    private String appLang;
    @Field("first_letter")
    private String firstLetter;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getDepEnglishName() {
        return depEnglishName;
    }

    public void setDepEnglishName(String depEnglishName) {
        this.depEnglishName = depEnglishName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionEnglishName() {
        return positionEnglishName;
    }

    public void setPositionEnglishName(String positionEnglishName) {
        this.positionEnglishName = positionEnglishName;
    }

    public String getSubordinateId() {
        return subordinateId;
    }

    public void setSubordinateId(String subordinateId) {
        this.subordinateId = subordinateId;
    }

    public String getOnboardingDate() {
        return onboardingDate;
    }

    public void setOnboardingDate(String onboardingDate) {
        this.onboardingDate = onboardingDate;
    }

    public String getAppLang() {
        return appLang;
    }

    public void setAppLang(String appLang) {
        this.appLang = appLang;
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }
}
