package club.bugmkaers.demospringbootmongodb.demo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @ClassName: BaseEntity
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 13:38
 * @Version: V1
 **/
public class BaseEntity {
    @Id
    private String id;
    @Field("created_by")
    private String createdBy;
    @Field("created_date")
    private String createdDate;
    @Field("last_modified_by")
    private String lastModifiedBy;
    @Field("last_modified_date")
    private String lastModifiedDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
}
