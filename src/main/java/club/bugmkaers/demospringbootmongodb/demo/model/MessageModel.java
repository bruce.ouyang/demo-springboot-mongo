package club.bugmkaers.demospringbootmongodb.demo.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @ClassName: MessageModel
 * @Description: TODO
 * @Author: bruce.ouyang
 * @Date: 2020/8/9 9:18 下午
 * @Version: v1.0
 */
@Data
@Document(collection = "message")
@Builder
public class MessageModel {

    @Id
    private String id;

    private String content;

    private String from;

    private String to;

}
