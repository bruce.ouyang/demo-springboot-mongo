package club.bugmkaers.demospringbootmongodb.demo.service;

import club.bugmkaers.demospringbootmongodb.demo.model.MessageModel;
import club.bugmkaers.demospringbootmongodb.demo.repository.MessageRepository;
import club.bugmkaers.demospringbootmongodb.helper.MessageModelTaskRunner;
import club.bugmkaers.demospringbootmongodb.helper.ThreadPoolHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static club.bugmkaers.demospringbootmongodb.constant.TaskConstant.*;

/**
 * @ClassName: MessageService
 * @Description: TODO
 * @Author: bruce.ouyang
 * @Date: 2020/8/9 9:24 下午
 * @Version: v1.0
 */
@Service
@Slf4j
public class MessageService {

    @Autowired
    private MessageRepository repository;

    public long saveEach() {

        long begin = System.currentTimeMillis();

        for (int i = 0; i < BATCH_SIZE; i ++ ) {
            repository.insert(MessageModel.builder()
                    .content("SaveEach: Hey, How do you do ~ ")
                    .from("bruce.each")
                    .to("Somebody" + i).build());
        }

        long cost = System.currentTimeMillis() - begin;

        log.info("save {} records each cost {} ms", BATCH_SIZE, cost);

        return cost;
    }

    public long saveBatch() {

        long begin = System.currentTimeMillis();

        ThreadPoolExecutor pool = ThreadPoolHelper.getPool();

        // 使用一个计数器跟踪完成的任务数
        AtomicInteger atomicInteger = new AtomicInteger();

        IntStream.rangeClosed(1, TASK_SIZE).forEach(i -> {
            int taskId = atomicInteger.incrementAndGet();
            try {
                pool.submit(new MessageModelTaskRunner(taskId, repository));
            } catch (Exception e) {
                e.printStackTrace();
                atomicInteger.decrementAndGet();
            }
        });

        log.info("提交任务数: {}", atomicInteger.intValue());


        long cost = System.currentTimeMillis() - begin;

        log.info("submit {} * {} records batch with thread-pool cost {} ms", TASK_SIZE, EACH_TASK_SIZE, cost);

        return cost;

    }

}
