package club.bugmkaers.demospringbootmongodb.demo.service;

import club.bugmkaers.demospringbootmongodb.demo.repository.MessageLogRepository;
import club.bugmkaers.demospringbootmongodb.helper.ModelGenerator;
import club.bugmkaers.demospringbootmongodb.helper.MessageLogTaskRunner;
import club.bugmkaers.demospringbootmongodb.helper.ThreadPoolHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static club.bugmkaers.demospringbootmongodb.constant.TaskConstant.EACH_TASK_SIZE;
import static club.bugmkaers.demospringbootmongodb.constant.TaskConstant.TASK_SIZE;

/**
 * @ClassName: MessageLogService
 * @Description: TODO
 * @Author: bruce.ouyang
 * @Date: 2020/8/9 9:24 下午
 * @Version: v1.0
 */
@Service
@Slf4j
public class MessageLogService {

    @Autowired
    private MessageLogRepository repository;

    /**
     * 单个任务逐行插入
     * @return
     */
    public long saveEach() {

        long begin = System.currentTimeMillis();

        for (int i = 0; i < EACH_TASK_SIZE; i ++ ) {
            repository.insert(ModelGenerator.messageLog("saveEach", i));
        }

        long cost = System.currentTimeMillis() - begin;

        log.info("save 100, 0000 records each cost {} ms", cost);

        return cost;
    }

    /**
     * 多任务批量插入
     * @return
     */
    public long saveBatch() {
        long begin = System.currentTimeMillis();

        ThreadPoolExecutor pool = ThreadPoolHelper.getPool();

        // 使用一个计数器跟踪完成的任务数
        AtomicInteger atomicInteger = new AtomicInteger();

        IntStream.rangeClosed(1, TASK_SIZE).forEach(i -> {
            int taskId = atomicInteger.incrementAndGet();
            try {
                pool.submit(new MessageLogTaskRunner(taskId, repository));
            } catch (Exception e) {
                e.printStackTrace();
                atomicInteger.decrementAndGet();
            }
        });

        log.info("提交任务数: {}", atomicInteger.intValue());


        long cost = System.currentTimeMillis() - begin;

        log.info("submit {} * {} records batch with thread-pool cost {} ms", TASK_SIZE, EACH_TASK_SIZE, cost);

        return cost;
    }
}
