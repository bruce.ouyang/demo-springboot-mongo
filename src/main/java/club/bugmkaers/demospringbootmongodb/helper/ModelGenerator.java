package club.bugmkaers.demospringbootmongodb.helper;

import club.bugmkaers.demospringbootmongodb.demo.model.Message;
import club.bugmkaers.demospringbootmongodb.demo.model.MessageLog;
import club.bugmkaers.demospringbootmongodb.demo.model.MessageModel;
import club.bugmkaers.demospringbootmongodb.demo.model.User;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;

/**
 * @ClassName: ModelGenerator
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 13:49
 * @Version: V1
 **/
public class ModelGenerator {

    /**
     * 生成 messageLog
     * @param prefix 前缀
     * @param i
     * @return
     */
    public static MessageLog messageLog(String prefix, int i) {

        String tenant = "BruceTest";

        // ----------------------------------- from User -----------------------------------

        String from = "Angelia-TEST-";
        String fromCN = "安吉莉亚-测试-";

        User fromUser = new User();
        fromUser.setEmpId(from + i);
        fromUser.setEnglishName(from + i);
        fromUser.setUserName(fromCN + i);
        fromUser.setDepEnglishName(from + i);
        fromUser.setDepName(fromCN + i);
        fromUser.setSex(1);
        fromUser.setTenantId(tenant);

        // ----------------------------------- to User -----------------------------------
        String to = "BRUCE-TEST-";
        String toCN = "布鲁斯-测试-";

        User toUser = new User();
        toUser.setEmpId(to + i);
        toUser.setEnglishName(to + i);
        toUser.setUserName(toCN + i);
        toUser.setDepEnglishName(to + i);
        toUser.setDepName(toCN + i);
        toUser.setSex(1);
        toUser.setTenantId(tenant);

        // ----------------------------------- message -----------------------------------
        JSONObject content = new JSONObject();
        content.put("title", prefix + ": 生日祝福");
        content.put("desc", prefix + ": 生日祝福");
        content.put("descEn", prefix + ": Happy Birthday");

        // ----------------------------------- message -----------------------------------
        Message msg = new Message();
        msg.setTenantId(tenant);

        msg.setIsActiveMsg(1);
        msg.setIsLoop(0);
        msg.setIsPraise(0);
        msg.setIsPush(0);
        msg.setIsTeam(0);

        msg.setMessage(content);

        msg.setToUser(toUser);
        msg.setFrom(fromUser);

        msg.setType(5);

        msg.setStartTime(DateUtil.format(DateUtil.yesterday(), "yyyy-MM-dd: HH:mm:ss"));
        msg.setEndTime(DateUtil.now());

        msg.setCreatedBy("Bruce");
        msg.setCreatedDate(DateUtil.now());
        msg.setLastModifiedBy("Bruce");
        msg.setLastModifiedDate(DateUtil.now());

        // ----------------------------------- message log -----------------------------------
        MessageLog messageLog  = new MessageLog();

        messageLog.setTenantId(tenant);
        messageLog.setEpTenantId(tenant);

        messageLog.setIsCopy(0);
        messageLog.setIsRead(0);
        messageLog.setIsReceive(0);
        messageLog.setIsTeam(0);

        messageLog.setToUser(toUser);
        messageLog.setCopyUser(null);
        messageLog.setMessage(msg);

        messageLog.setCreatedBy("Bruce");
        messageLog.setCreatedDate(DateUtil.now());
        messageLog.setLastModifiedBy("Bruce");
        messageLog.setLastModifiedDate(DateUtil.now());

        return messageLog;
    }

    /**
     * 生成 messageModel
     * @param prefix
     * @param i
     * @return
     */
    public static MessageModel messageModel(String prefix, int i) {
        return MessageModel.builder()
                .content(prefix + ": Hey, How do you do ~ ")
                .from("bruce." + prefix)
                .to("Somebody" + i).build();
    }
}
