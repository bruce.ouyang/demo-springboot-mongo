package club.bugmkaers.demospringbootmongodb.helper;

import cn.hutool.core.thread.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @ClassName: ThreadPoolHelper
 * @Description: TODO
 * @Author: bruce.ouyang
 * @Date: 2020/8/9 11:26 下午
 * @Version: v1.0
 */
@Slf4j
public class ThreadPoolHelper {

    private static final Integer corePoolSize = 10;
    private static final Integer maxPoolSize = 50;
    private static final Integer keepAliveTime = 10;
    private static final BlockingQueue blockingQueue = new ArrayBlockingQueue<>(1000);
    private static final ThreadFactory threadFactory = new ThreadFactoryBuilder().setNamePrefix("bruce-").build();
    private static final ThreadPoolExecutor pool;

    static{
        pool = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, blockingQueue, threadFactory);
        printPoolStats(pool);
    }

    public static ThreadPoolExecutor getPool() {
        return pool;
    }


    public static void printPoolStats(ThreadPoolExecutor threadPool) {
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            log.info("=========================");
            log.info("Pool Size: {}", threadPool.getPoolSize());
            log.info("Active Threads: {}", threadPool.getActiveCount());
            log.info("Number of Tasks Completed: {}", threadPool.getCompletedTaskCount());
            log.info("Number of Tasks in Queue: {}", threadPool.getQueue().size());

            log.info("=========================");
        }, 0, 1, TimeUnit.SECONDS);
    }
}
