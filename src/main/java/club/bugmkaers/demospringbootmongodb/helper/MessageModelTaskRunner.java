package club.bugmkaers.demospringbootmongodb.helper;

import club.bugmkaers.demospringbootmongodb.demo.model.MessageModel;
import club.bugmkaers.demospringbootmongodb.demo.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static club.bugmkaers.demospringbootmongodb.constant.TaskConstant.BATCH_SIZE;
import static club.bugmkaers.demospringbootmongodb.constant.TaskConstant.EACH_TASK_SIZE;

/**
 * @ClassName: MessageLogTaskRunner
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 14:17
 * @Version: V1
 **/
@Slf4j
public class MessageModelTaskRunner implements Runnable {

    private int taskId;
    private MessageRepository repository;

    public MessageModelTaskRunner(int taskId, MessageRepository repository) {
        this.taskId = taskId;
        this.repository = repository;
    }

    @Override
    public void run() {
        long begin = System.currentTimeMillis();
        log.info("start 第 {} 个 task...", taskId);

        List<MessageModel> list = new ArrayList<>();

        for (int i = 0; i < EACH_TASK_SIZE; i ++ ) {
            list.add(ModelGenerator.messageModel("PoolExecutor", i));
            if (list.size() == BATCH_SIZE) {
                repository.insert(list);
                list.clear();
            }
        }

        long cost = System.currentTimeMillis() - begin;

        log.info("finish 第 {} 个 task, cost {} ms", taskId, cost);
    }
}
