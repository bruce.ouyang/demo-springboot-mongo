package club.bugmkaers.demospringbootmongodb.constant;

/**
 * @ClassName: TaskConstant
 * @Description: TODO
 * @Author: Bruce.Ouyang
 * @Date: 2020/8/10 14:05
 * @Version: V1
 **/
public class TaskConstant {

    /**
     * 任务数
     */
    public final static int TASK_SIZE = 100;

    /**
     * 每个任务插入行数
     */
    public final static int EACH_TASK_SIZE = 100000;

    /**
     * 批量大小
     */
    public final static int BATCH_SIZE = 2000;
}
