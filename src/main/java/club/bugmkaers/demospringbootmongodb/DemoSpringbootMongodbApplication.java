package club.bugmkaers.demospringbootmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author Bruce.OuYang
 */
@SpringBootApplication
public class DemoSpringbootMongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringbootMongodbApplication.class, args);
    }

}
